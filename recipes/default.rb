#
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

include_recipe 'ingredients'

#
# Create the PostgreSQL configuration directory.
#
directory '/etc/postgresql'

postgresql.versions.each do |version, v|

  #
  # Creating a file at +/etc/postgresql/{version}+ prevents +dpkg+ from creating
  # a default cluster for us, but it must be deleted immediately after
  # +postgresql+ is installed.
  #
  file "/etc/postgresql/#{version}" do
    backup false
    group  'postgres'
    mode   0644
    owner  'postgres'

    action :create_if_missing
  end

  #
  # PostgreSQL server package.
  #
  package "postgresql-#{version}" do
    notifies :delete, "file[/etc/postgresql/#{version}]", :immediately
  end

  v.clusters.each do |cluster, c|

    #
    # Create each database cluster.
    #
    execute "pg_createcluster:#{version}:#{cluster}" do
      not_if do
        File.exists? "/etc/postgresql/#{version}/#{cluster}/postgresql.conf"
      end

      command c.create_command
    end

    #
    # Host-based authentication configuration for each cluster.
    #
    template "/etc/postgresql/#{version}/#{cluster}/pg_hba.conf" do
      group     c.group
      mode      0644
      owner     c.user
      source    'pg_hba.conf.erb'
      variables cluster: cluster, hba: c.hba, version: version

      notifies :restart, 'service[postgresql]'
    end

    #
    # Main configuration for each cluster.
    #
    template "/etc/postgresql/#{version}/#{cluster}/postgresql.conf" do
      group     c.group
      mode      0644
      owner     c.user
      source    'postgresql.conf.erb'
      variables cluster: c, version: v

      notifies :restart, 'service[postgresql]'
    end

    #
    # Trusted root for client certificates.
    #
    file "#{c.data_directory}/root.crt" do
      not_if {c.root_certificate.nil?}

      content c.root_certificate
      group   c.group
      mode    0644
      owner   c.user

      notifies :restart, 'service[postgresql]'
    end

    #
    # Certificate Revocation List for client certificates.
    #
    file "#{c.data_directory}/root.crl" do
      not_if {c.root_crl.nil?}

      content c.root_crl
      group   c.group
      mode    0644
      owner   c.user

      notifies :restart, 'service[postgresql]'
    end

    #
    # Server certificate for SSL connections.
    #
    file "#{c.config[:data_directory]}/server.crt" do
      not_if {c.server_certificate.nil?}

      content c.server_certificate
      group   c.group
      mode    0644
      owner   c.user

      notifies :restart, 'service[postgresql]'
      subscribes(
          :delete,
          resources("execute[pg_createcluster:#{version}:#{cluster}]"),
          :immediately)
    end

    #
    # Private key for SSL connections.
    #
    file "#{c.config[:data_directory]}/server.key" do
      not_if {c.server_key.nil?}

      content c.server_key
      group   c.group
      mode    0600
      owner   c.user

      notifies :restart, 'service[postgresql]'
      subscribes(
          :delete,
          resources("execute[pg_createcluster:#{version}:#{cluster}]"),
          :immediately)
    end

    c.roles.each do |id, role|
      #
      # CREATE ROLE for every user.
      #
      postgresql_ingredients_script "create_role:#{version}:#{cluster}:#{id}" do
        cluster cluster
        command role.create_command
        version version

        action :nothing
        subscribes(
            :run,
            resources("execute[pg_createcluster:#{version}:#{cluster}]"))
      end
    end

    c.databases.each do |id, database|
      #
      # CREATE DATABASE for every database.
      #
      postgresql_ingredients_script(
          "create_database:#{version}:#{cluster}:#{id}") do
        cluster cluster
        command database.create_command
        version version

        action :nothing
        subscribes(
            :run,
            resources("execute[pg_createcluster:#{version}:#{cluster}]"))
      end
    end
  end
end

#
# Manages all PostgreSQL servers.
#
service 'postgresql' do
  supports :'force-reload' => true,
           :reload         => true,
           :restart        => true,
           :start          => true,
           :status         => true,
           :stop           => true
end
