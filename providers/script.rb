#
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

include Chef::Mixin::ShellOut
include Chef::Mixin::Template

#
# Evaluate a PostgreSQL script with +psql+. For full documentation, see the
# README.
#
action :run do
  version = new_resource.version
  v = postgresql.versions[version]
  cluster = new_resource.cluster
  c = v.clusters[cluster]

  group = new_resource.group || c.group
  user = new_resource.user || c.user

  if new_resource.template
    context = {node: node}.merge! new_resource.variables
    render_template IO.read(template_location), context do |template|
      shell_out! "psql --cluster #{version}/#{cluster} --file -",
                 group: group, input: IO.read(template.path), user: user
    end
  else
    shell_out! "psql --cluster #{version}/#{cluster} --file -",
               group: group, input: new_resource.command, user: user
  end
end

private

#
# Find a template and return its path.
#
def template_location
  return @template_location if instance_variable_defined? :@template_location
  cookbook = run_context.cookbook_collection[new_resource.cookbook_name]
  cookbook.preferred_filename_on_disk_location node, :templates,
                                               new_resource.template
end
