#
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

Ingredients.for_cookbook :postgresql do
  data_bag_item 'services', 'postgresql'

  named_collection :versions, default: '8.4' do
    named_collection :clusters, default: 'main' do
      attribute :data_directory,        default: proc {"/var/lib/postgresql/#{parent.name}/#{name}"}
      attribute :group,                 default: 'postgres'
      attribute :listen_addresses,      default: ['localhost']
      attribute :locale,                default: 'en_US.utf8'
      attribute :logfile,               default: proc {"/var/log/postgresql/postgresql-#{parent.name}-#{name}.log"}
      attribute :port,                  default: 5432
      attribute :unix_socket_directory, default: '/var/run/postgresql'
      attribute :user,                  default: 'postgres'

      data_bag_attribute :root_certificate
      data_bag_attribute :root_crl
      data_bag_attribute :server_certificate
      data_bag_attribute :server_key

      def create_command
        options = {
          datadir:   :data_directory,
          group:     :group,
          locale:    :locale,
          logfile:   :logfile,
          port:      :port,
          socketdir: :unix_socket_directory,
          user:      :user
        }.collect{|flag, key| "--#{flag} '#{config[key]}'"}.join(' ')

        "pg_createcluster #{options} --start #{parent.name} #{name}"
      end

      named_collection :databases do
        attribute :configuration,    default: {}
        attribute :connection_limit
        attribute :encoding
        attribute :locale
        attribute :owner
        attribute :template

        def create_command
          options = ["CREATE DATABASE #{name}"]
          unless connection_limit.nil?
            options << "CONNECTION LIMIT #{connection_limit}"
          end
          options << "ENCODING '#{encoding}'" unless encoding.nil?
          unless locale.nil?
            options << "LC_COLLATE '#{locale}'"
            options << "LC_CTYPE '#{locale}'"
          end
          options << "OWNER #{owner}"         unless owner.nil?
          options << "TEMPLATE #{template}"   unless template.nil?
          commands = ["#{options.join(' ')};"]
          configuration.each do |parameter, value|
            commands << "ALTER DATABASE #{name} SET #{parameter} = #{value};"
          end

          commands.join "\n"
        end
      end

      ordered_collection :hba do
        attribute :cidr_address
        attribute :databases,    default: ['all']
        attribute :method,       default: 'md5'
        attribute :types,        default: ['host']
        attribute :users,        default: ['all']
      end

      search_collection :roles, as: :role, sources: [:people, :services] do
        attribute :admin,            default: []
        attribute :connection_limit
        attribute :createdb,         default: false
        attribute :createrole,       default: false
        attribute :inherit,          default: true
        attribute :in_role,          default: []
        attribute :login,            default: false
        attribute :password
        attribute :role,             default: []
        attribute :superuser,        default: false
        attribute :valid_until

        def create_command
          options = ["CREATE ROLE #{name}"]
          options << "ADMIN #{admin.join(', ')}"     unless admin.empty?
          unless connection_limit.nil?
            options << "CONNECTION LIMIT #{connection_limit}"
          end
          options << 'CREATEDB'                      if     createdb
          options << 'CREATEROLE'                    if     createrole
          options << "IN ROLE #{in_role.join(', ')}" unless in_role.empty?
          options << 'LOGIN'                         if     login
          options << 'NOINHERIT'                     unless inherit
          options << "PASSWORD '#{password}'"        unless password.nil?
          options << "ROLE #{role.join(', ')}"       unless role.empty?
          options << 'SUPERUSER'                     if     superuser
          options << "VALID UNTIL #{valid_until}"    unless valid_until.nil?

          "#{options.join(' ')};"
        end
      end
    end
  end
end
